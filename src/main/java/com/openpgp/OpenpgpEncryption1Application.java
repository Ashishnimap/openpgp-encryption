package com.openpgp;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

import com.didisoft.pgp.PGPException;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class })

public class OpenpgpEncryption1Application {

	public static void main(String[] args) throws PGPException, IOException {
		SpringApplication.run(OpenpgpEncryption1Application.class, args);

	}

}
