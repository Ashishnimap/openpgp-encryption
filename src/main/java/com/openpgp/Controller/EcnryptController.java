package com.openpgp.Controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.didisoft.pgp.PGPException;
import com.openpgp.service.EncryptService;

@RestController
//@RequestMapping("/encrypt")
public class EcnryptController {

	@Autowired
	private EncryptService encryptService;

	@GetMapping("/encrypt")
	public ModelAndView encrypt(Model model, String payload) throws PGPException, IOException {
		if (payload == null) {
			payload = null;

		}
		String encryptedString = encryptService.encrypt(payload);
		model.addAttribute("payload", payload);
		model.addAttribute("encryptedString", encryptedString);
		ModelAndView andView = new ModelAndView();
		andView.setViewName("index");
		return andView;
	}
}
