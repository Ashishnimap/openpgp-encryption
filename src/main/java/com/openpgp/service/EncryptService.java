package com.openpgp.service;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.didisoft.pgp.PGPException;
import com.didisoft.pgp.PGPLib;

@Service
public class EncryptService {

	@Value("classpath:static/privateKey.asc")
	private Resource privateKeyFile;

	@Value("classpath:static/publicKey.asc")
	private Resource publicKeyFile;

	@Value("${password}")
	private String password;

	PGPLib pgp = new PGPLib();
	boolean asciiArmor = true;
	boolean withIntegrityCheck = false;

	public String encrypt(String payload) throws PGPException, IOException {
		File file = privateKeyFile.getFile();

		File file2 = publicKeyFile.getFile();

		String privateKey = file.getPath();

		String publicKey = file2.getPath();

		String a = pgp.signAndEncryptString(payload, privateKey, password, publicKey);
		String enc = a.replaceAll("\\r\\n", "");
		return enc;
	}
}
